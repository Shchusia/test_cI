from flask import Flask
from flask import render_template
from flask import request


app = Flask(__name__)


def test():
    return render_template('test.html')


def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


def shutdown():
    shutdown_server()
    return 'Server shutting down...'


app.add_url_rule('/test', 'test', test)
app.add_url_rule('/shutdown', 'shutdown', shutdown)